<?php include "./php/head.php" ?>

<span class="center">
    <h1>Данные человека</h1>
    <?php
    $id = $_POST['id'];

    $user = db_getById('people_list',$id);

    printf("<p>ПIБ: ".$user['surname']." ".$user['name']." ".$user['for_full_name']."</p>");
    printf("<p>дата народження: ".$user['birthday']."</p>");
    printf("<p>Місце проживання (населений пункт): ".$user['home']."</p>");
    printf("<p>Місце роботи, посада: ".$user['work']."</p>");
    printf("<p>Науковий ступінь: ".$user['rank']."</p>");
    printf("<p>Вчене звання: ".$user['academic_rank']."</p>");
    printf("<p>Наукова спеціальність: ".$user['specialization']."</p>");
    printf("<p>Контактний телефон: ".$user['number']."</p>");
    printf("<p>E-mail: ".$user['email']."</p>");

    if ($user['academician'] !== null ) {
        printf("<p>Статус: академік</p>");
    } elseif ($user['correspondent'] !== null) {
        printf("<p>Статус: член кореспондент</p>");
    } else {
        printf("<p>Статус: не визначенно</p>");
    }

    printf("<p>Номер посвідчення: ".$user['id_number']."</p>");
    printf("<p>Номер рішення: ".$user['decision_number']."</p>");
    printf("<p>Дата рішення: ".$user['decision_date']."</p>");


    if ($_SESSION['admin'] == true){
        printf('<form class="allform" method="post" action="./php/people_list.php"><input type="hidden" name="id" value="'.$id.'"><input id="submit" class="button" type="submit" value="редактировать"><br/></form>');
        printf('<form class="allform" method="post" action="./php/delete.php"><input type="hidden" name="id" value="'.$id.'"><input id="submit" class="button" type="submit" value="УДАЛИТЬ"><br/></form>');
    }
    ?>
</span>


<?php include "./php/footer.php" ?>
