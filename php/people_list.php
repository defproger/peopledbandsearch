<?php
require 'bd.php';
require 'functions.php';

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Админ</title>
    <!-- css -->
    <link rel="stylesheet" href="../css/main.css">
</head>
<body>
<header>
    <div class="container">

        <div class="headerin">
            <div class="logohead">
                <img class="modlogo" src="../img/logo.png" alt="">
            </div>
            <nav class="navhead">
                <a class="navlink" href="add.php">Добавить</a>
                <a class="navlink" href="index.php">Найти</a>
            </nav>
            <nav class="navhead">
                <a class="navlink activelink" href="./php/log.php">Перезайти</a>
            </nav>
        </div>
    </div>
</header>
<?php

$id = $_POST['id'];

$user = db_getById('people_list',$id);
?>
<center>
    <h1>РЕДАКТИРОВАТЬ ПЕРЕД ДОБАВЛЕНИЕМ</h1>
    <form method="post" target='_blank' action="update_global.php">
        <input type='hidden' name='id' value='<?php echo $id;?>'>
        <input type="text" placeholder="Имя" value="<?php echo $user['name'];?>" name="name" size="30">
        <input type="text" class="justpadding" value="<?php echo $user['academic_rank'];?>" placeholder="учённое звание" name="academic_rank" size="30"><br/>
        <input type="text" placeholder="Фамилия" value="<?php echo $user['surname'];?>" name="surname" size="30">
        <input type="text" class="justpadding" value="<?php echo $user['home'];?>" placeholder="место жительства" name="home" size="30"><br/>
        <input type="text" placeholder="Отчество" value="<?php echo $user['for_full_name'];?>" name="full_name" size="30">
        <span>
            <h2>Статус</h2>
            <input type="checkbox" <?php if ($user['academician'] !== null ) {printf("checked");}?> class="justpadding" placeholder="статус" name="academician"> академік
            <input type="checkbox" <?php if ($user['correspondent'] !== null ) {printf("checked");}?> class="justpadding" placeholder="статус" name="correspondent"> член кореспондент
        </span><br>
        День рождения<input type="date" value="<?php echo $user['birthday'];?>" name="full_name" placeholder="День рождения" name="birthday" size="30">
        <input type="text"  value="<?php echo $user['rank'];?>" placeholder="Науковий ступінь" name="rank"class="justpadding" size="30"><br>
        <input type="text"  value="<?php echo $user['specialization'];?>" placeholder="Наукова спеціальність" name="specialization" size="30">
        <input type="tel"  value="<?php echo $user['number'];?>" placeholder="Номер телефона" name="number"class="justpadding" size="30"><br>
        <input type="email"  value="<?php echo $user['email'];?>" placeholder="Почта" name="email" size="30">
        <input type="text"  value="<?php echo $user['id_number'];?>" placeholder="Номер посвідчення" name="id_number"class="justpadding" size="30"><br>
        Дата рішення<input  value="<?php echo $user['decision_date'];?>" type="date" placeholder="Дата рішення" name="decision_date" size="30">
        <input type="text"  value="<?php echo $user['decision_number'];?>" placeholder="Номер рішення" class="justpadding" name="decision_number" size="30"><br>
        <input type="text"  value="<?php echo $user['work'];?>" placeholder="Місце роботи, посада" name="work" size="30"><br>


        <div class="btn">
            <input id="submit" class="button" type="submit" value="Обновить информацию"><br/>
        </div>
    </form>
</center>


<?php include "footer.php" ?>
