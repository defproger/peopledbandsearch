<?php
session_start();

require 'bd.php';
require 'functions.php';

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Поиск людей</title>
	<!-- css -->
	<link rel="stylesheet" href="./css/main.css">
  </head>
  <body>
  <header>
      <div class="container">

          <div class="headerin">
              <div class="logohead">
                  <img class="modlogo" src="img/logo.png" alt="">
              </div>
              <nav class="navhead">
                  <a class="navlink" href="add.php">Добавить</a>
                  <a class="navlink" href="index.php">Найти</a>
              </nav>
              <nav class="navhead">
                  <a class="navlink activelink" href="./php/log.php">Войти</a>
              </nav>
          </div>
      </div>
  </header>
